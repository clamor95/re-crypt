#!/usr/bin/env python3
#
# re-crypt
#
# This file is part of re-crypt tool
# Copyright (C) 2023 Svyatoslav Ryhel <clamor95@gmail.com>
# Copyright (C) 2023 Ion Agorria <ion@agorria.com>
#

import os
import struct
import argparse

from struct import unpack

# SDRAM timings on T30
# class SDRAM_PARAMS(Structure):
#  _fields_ = [
#    ("memory_type", c_uint),
#    ("timings", c_uint * 191)   # 0x14c + 192 * 4 or 0xc0 * num
#  ]
#
#    ("num_sdram_sets", c_uint),         # 0x144 offset
#    ("sdram_params", SDRAM_PARAMS * 4)  # 0x148 offset 192 * 4 size

parser = argparse.ArgumentParser(description='SDRAM timings decompiller')
parser.add_argument('bct', metavar='bct', type=str,
                    help='path to the bct binary')
parser.add_argument('--t20', dest='t20', action='store_true',
                    help='Tegra20 SOC')
parser.add_argument('--t30', dest='t30', action='store_true',
                    help='Tegra30 SOC')
parser.add_argument('--t124', dest='t124', action='store_true',
                    help='Tegra124 SOC')

arguments = parser.parse_args()

bct_path = os.path.expanduser(arguments.bct)
if not os.path.isfile(bct_path):
    print("Invalid bct path specified!")
    exit(1)

with open(bct_path, 'rb') as file:
    buffer = file.read()

if arguments.t20:
    num_sdram_sets = buffer[0x84]
elif arguments.t124:
    num_sdram_sets = buffer[0x7f4]
else:
    num_sdram_sets = buffer[0x144]

# check for timings even if none are declared
if num_sdram_sets == 0:
    num_sdram_sets = 4

for i in range(num_sdram_sets):
    if arguments.t20:
        timing_start = 0x8c + 0x200 * i
        timimg_end = 0x21c + 0x200 * i
        timings = list(unpack('<100I', buffer[timing_start:timimg_end]))
    elif arguments.t124:
        timing_start = 0x7fc + 0x4d4 * i
        timimg_end = 0xccc + 0x4d4 * i
        timings = list(unpack('<308I', buffer[timing_start:timimg_end]))
    else:
        timing_start = 0x14c + 0x300 * i
        timimg_end = 0x448 + 0x300 * i
        timings = list(unpack('<191I', buffer[timing_start:timimg_end]))
    print("TIMING FOR SDRAM", i)
    print("        ", end="")
    for x in range(len(timings)):
        if 0 < x and (x % 4) == 0:
            print("")
            print("        ", end="")
        print(("\"0x{:08x}\", ").format(timings[x]), end="")
    print("")
    print("")

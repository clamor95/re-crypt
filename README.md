# re-crypt
A tool for re-partitioning Tegra 2 and Tegra 3 based production devices.

re-crypt is a small tool that allows to replace the vendor designed TegraPT with more generic one that works better with U-Boot and Linux overall. 

## Problems of Tegra 2/3/4 product devices:
- use of custom TegraPT that is not widely supported and conflicts with default setups
- use of proprietary bootloader which was not updated for over a decade
- use of AES encryption for BCT (boot configuration table) and EBT (bootloader)

Last one is not a problem but rather an unfortunate measure which makes devs life harder.

## Solutions
- perform a re-partiton to match default setup and primary partitions of TegraPT, this will allow to use all mmcblk0 block as GPT storage without risk of breaking bootloader
- switch to open source bootloader U-Boot which still has quite strong Tegra support
- store AES encryption key in BCT region to be able to update bootloader without host PC (not ideal but tegra SE is not documented to be properly implemented)

## Implementation
re-crypt script performs bootloader encryption, BCT generation and encryption as well as packing them into primary 4MB block (or 2MB _bct_ and _ebt_ images). Then this block can be flashed via nv3p or pre-loaded U-Boot.

## Usage
You need to have a U-Boot for your device built pre-emptively. Clone this repo, place your U-Boot inside it (same level as re-crypt).
re-crypt supports next arguments:
- `--dev` to pass device name (mandatory)
- `--sbk` to define SBK of your device (optional for some devices)
- `--bl` to set name of your bootloader (optional, default name is `u-boot-dtb-tegra.bin`)
- `--split` to produce bct.img and ebt.img for pre-loaded U-Boot flashing

### List of supported devices
Sorted alphabetically by codename

| Device  | Codename | Note |
| - | - | - |
| Acer Iconia Tab A500 | a500 | Requires `--sbk` |
| Acer Iconia Tab A510 | a510 | Requires `--sbk` |
| Acer Iconia Tab A701 | a701 | Requires `--sbk` |
| Pegatron Chagall | chagall | Requires `--sbk` |
| HTC One X | endeavoru | No need in `--sbk` |
| HTC One X+ | enrc2b | No need in `--sbk` |
| ASUS/Google Nexus 7 (2012) | grouper | Requires `--sbk` |
| Lenovo Ideapad Yoga 11 | ideapad-yoga-11 | Requires `--sbk` |
| Xiaomi Mi Pad | mocha | No need in `--sbk`, output is split |
| Ouya Ouya | ouya | Requires `--sbk` |
| Toshiba AC100/Dynabook AZ | paz00 | No need in `--sbk` |
| ASUS Transformer AiO P1801-T | p1801-t | Requires `--sbk` |
| LG Optimus 4X HD | p880 | No need in `--sbk` |
| LG Optimus Vu | p895 | No need in `--sbk` |
| Wexler Tab 7t | qc750 | No need in `--sbk` |
| Microsoft Surface RT | surface-rt | Requires `--sbk` and SPI flash swap |
| ASUS Eee Pad Transformer TF101 | tf101 | Only for SBK 1 version, no need in `--sbk` |
| ASUS Transformer Prime TF201 | tf201 | Requires `--sbk` |
| ASUS Transformer Pad TF300T | tf300t | Requires `--sbk` |
| ASUS Transformer Pad 3G TF300TG | tf300tg | Requires `--sbk` |
| ASUS Transformer Pad LTE TF300TL | tf300tl | Requires `--sbk` |
| ASUS VivoTab RT TF600T | tf600t | Requires `--sbk` |
| ASUS Transformer Infinity TF700T | tf700t | Requires `--sbk` |
| ASUS/Google Nexus 7 3G (2012) | tilapia | Requires `--sbk` |

Example of command call for ASUS Transformer Prime TF201

`./re-crypt.py --dev tf201 --sbk <your sbk> --bl u-boot.bin`

where `<your sbk>` has next form `0xXXXXXXXX 0xXXXXXXXX 0xXXXXXXXX 0xXXXXXXXX`

## Credits
- [CrackTheSurface](https://github.com/CrackTheSurface) for initial [firmware cryptography](https://openrt.gitbook.io/open-surfacert/surface-rt/firmware/encrypt-firmware)
- [Ion Agorria](https://github.com/IonAgorria) for help with Python implementation and [HTC-One-X-Depth-Charge](https://github.com/IonAgorria/HTC-One-X-Depth-Charge)

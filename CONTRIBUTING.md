# re-crypt
A tool for re-partitioning Tegra 2 and Tegra 3 based production devices.

re-crypt is a small tool that allows to replace vendor designed TegraPT with a more generic one that works better with U-Boot and Linux overall. 

## Improving generation
Any improvements to generating mechanisms are welcome, but will be carefully checked before merging!

## Additional devices support
Any Tegra 3 device with a non-permanent bootloader flased can be supported by re-crypt. Only Tegra 2 devices with the ability to use NvFlash can be supported by re-crypt. Tegra K1 is supported only if device is unfused.

To add your device support, you only need to propose a `<device name>.json` description of your device. Such a file can be generated semi-automatically using the `bct_dump` tool and provided by the re-crypt timing generator (`timing.py`) from `bct` file of your device.

General algorithm:
 - dump BCT from your device
 - use `bct_dump` to get most of the data about the device
 - use `timing.py` (with `--t20`, `--t30` or `--t124` arguments) to generate SDRAM timings.

Namings in the `.json` file mostly match those that appear in `bct_dump`.

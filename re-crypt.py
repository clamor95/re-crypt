#!/usr/bin/env python3
#
# re-crypt
#
# Copyright (C) 2023 Svyatoslav Ryhel <clamor95@gmail.com>
# Copyright (C) 2023 Ion Agorria <ion@agorria.com>
#

import os
import argparse
import bct
import crypto
import struct
import shutil

from bct import create_bct, get_soc
from shutil import rmtree

# Known keys
ENDEAVORU = ['0x588F67E6', '0x6C763FF9', '0x4A74B979', '0x24B3A499']
ENRC2B = ['0x488A2827', '0x15411737', '0xB81E0F92', '0x4DBDEC47']
P880 = ['0xC18269EE', '0x0900CE58', '0x482ABE34', '0xBFF1BC01']
P895 = ['0x950821AD', '0x0964CE58', '0xFE98BE34', '0xBFF1AC02']
TF101v1 = ['0x1682CCD8', '0x8A1A43EA', '0xA532EEB6', '0xECFE1D98']
NOSBK = ['0x00000000', '0x00000000', '0x00000000', '0x00000000']

# List of supported devices
devlist = ['tf101', 'tf201', 'tf300t', 'tf300tg', 'tf600t', 'tf700t',
           'p1801-t', 'p880', 'p895', 'grouper', 'chagall', 'endeavoru',
           'a510', 'a701', 'qc750', 'surface-rt', 'paz00', 'ouya',
           'ideapad-yoga-11', 'tilapia', 'a500', 'enrc2b', 'mocha',
           'tf300tl']

parser = argparse.ArgumentParser(description='A tool for re-partitioning \
                                 Tegra 2 and Tegra 3 based production devices')
parser.add_argument('--dev', metavar='device', type=str, required=True,
                    default=None, choices=devlist,
                    help='device code name, check README')
parser.add_argument('--bl', metavar='bootloader', type=str,
                    default="u-boot-dtb-tegra.bin",
                    help='path to the bootloader binary')
parser.add_argument('--sbk', metavar='sbk', type=str, default=None,
                    nargs=4, help="Security Boot Key of your device")
parser.add_argument('--split', dest='split', action='store_true',
                    help='Export repart-block as 2 separated images')

args = parser.parse_args()

# Set SBK for known devices
if args.dev == "endeavoru":
    args.sbk = ENDEAVORU
elif args.dev == "enrc2b":
    args.sbk = ENRC2B
elif args.dev == "p880":
    args.sbk = P880
elif args.dev == "p895":
    args.sbk = P895
elif args.dev == "tf101":
    args.sbk = TF101v1
elif args.dev in ("qc750", "paz00", "mocha"):
    args.sbk = NOSBK

# Define the size of mmcblk0boot0/1 partitions is bytes
if args.dev in ("mocha"):
    bootsize = 0x400000
    args.split = True
if args.dev in ("a500"):
    bootsize = 0x100000
else:
    bootsize = 0x200000

if args.sbk is None:
    print("[re-crypt] SBK is not specified!")
    exit(1)

bl_path = os.path.expanduser(args.bl)
if not os.path.isfile(bl_path):
    print("[re-crypt] Invalid bootloader path specified!")
    exit(1)

# Convert SBK from string to byte form
sbk_str = ''
for i in range(4):
    sbk_frg = args.sbk[i]
    if sbk_frg.startswith("0x"):
        sbk_frg = sbk_frg[2:]
    sbk_str += sbk_frg
sbk_str = sbk_str.lower()
if len(sbk_str) != 32:
    print("[re-crypt] Please provide valid hex SBK key (4 sets 8 char each)")
sbk = []
for i in range(16):
    by = sbk_str[i*2:i*2+2]
    by = int(by, 16)
    sbk.append(by)
sbk_byte = struct.pack("B" * 16, *sbk)

# Load and encrypt bootloader
with open(bl_path, 'rb') as f:
    bl_data = bytes(f.read())
    while len(bl_data) % 16 != 0:
        bl_data += b'\0'

print("[re-crypt] Bootloader prepared...")

if sbk_byte != bytes(16):
    bl_enc = crypto.encrypt_verify(bl_data, sbk_byte)
else:
    bl_enc = bl_data

bl_hash = crypto.hash_aes_cmac(bl_enc, sbk_byte)

bl_len = len(bl_enc)
bl_enc = bl_enc + b'\0' * (bootsize - bl_len)

print("[re-crypt] Bootloader encrypted...")

soc = get_soc(args.dev)

# Generate and encrypt BCT
bct_config = {
    'device': args.dev,
    'length': bl_len,
    'crypto_hash': struct.unpack("<4I", bl_hash),
}

bct_data = create_bct(bct_config, bootsize)
if soc in ('t20', 't30'):
    sbct_data = bct_data[0x10:]
elif soc in ('t124'):
    ubct_data = bct_data[:0x6b0]
    sbct_data = bct_data[0x6b0:]

print("[re-crypt] BCT created...")

if sbk_byte != bytes(16):
    bct_enc = crypto.encrypt_verify(sbct_data, sbk_byte)
else:
    bct_enc = sbct_data

bct_hash = crypto.hash_aes_cmac(bct_enc, sbk_byte)

if soc in ('t20', 't30'):
    bct_enc = bct_hash + bct_enc + sbk_byte
elif soc in ('t124'):
    ubct_data = bytearray(ubct_data)
    struct.pack_into("B" * 16, ubct_data, 0x310, *bct_hash)
    bct_enc = ubct_data + bct_enc + sbk_byte

bct_len = len(bct_enc)
bct_enc = bct_enc + b'\0' * (bootsize - bct_len)

print("[re-crypt] BCT encrypted...")

if args.split:
    with open('bct.img', 'wb') as output:
        output.write(bytes(bct_enc))
    with open('ebt.img', 'wb') as output:
        output.write(bytes(bl_enc))
    print("[re-crypt] bct.img and ebt.img are ready!")
else:
    repart_block = bct_enc + bl_enc
    with open('repart-block.bin', 'wb') as output:
        output.write(bytes(repart_block))
    print("[re-crypt] repart-block is ready!")

rmtree("__pycache__")
